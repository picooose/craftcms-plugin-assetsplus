# Assets Plus plugin for Craft CMS 3.x

Assets Plus

## Requirements

This plugin requires Craft CMS 3.0 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require apstudio/assetsplus

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Assets Plus.