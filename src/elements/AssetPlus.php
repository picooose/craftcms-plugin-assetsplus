<?php

namespace apstudio\assetsplus\elements;

use apstudio\assetsplus\services\AssetsPlusService;

use yii\base\Event;
use craft\elements\Asset;
use craft\services\Revisions;

class AssetPlus extends Asset{

  public $width;
  public $height;
  public $groupRatio;

  // Public Properties
  // =========================================================================
  public function init(){
    parent::init();

    //Events
    Event::on(Revisions::class, Revisions::EVENT_AFTER_REVERT_TO_REVISION, function($e){
      $this->revertToRevision($e->source, $e->revision);
    });
  }

  public static function displayName(): string{
    return 'Asset Plus';
  }

  public function getRatio(){
    return $this->width / $this->height;
  }

  public function getFormat(){
    return $this->width > $this->height ? 'landscape' : 'portrait';
  }

  // Protected Properties
  // =========================================================================
  protected function revertToRevision($element, $revision){
    $revisionGroups = AssetsPlusService::getGroups($revision);
    AssetsPlusService::saveGroups($element, $revisionGroups);
  }
}
