<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      https://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus;

use apstudio\assetsplus\models\Settings;
use apstudio\assetsplus\services\AssetsPlusService as AssetsPlusServiceService;
use apstudio\assetsplus\fields\AssetsPlusField as AssetsPlusField;

use craft\base\Plugin;
use craft\elements\Asset;
use craft\events\ModelEvent;
use craft\services\Assets;
use craft\events\AssetThumbEvent;
use craft\helpers\Assets as AssetsHelper;
use craft\services\Fields;
use craft\events\RegisterComponentTypesEvent;
use yii\base\Event;

/**
 * Class AssetsPlus
 *
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 *
 * @property  AssetsPlusServiceService $assetsPlusService
 */
class AssetsPlus extends Plugin{
  public static $plugin;

  // Public Properties
  // =========================================================================

  public $schemaVersion = '1.0.0';

  public function init(){
    parent::init();
    self::$plugin = $this;

    $this->installEventHandlers();
  }

  // Protected Methods
  // =========================================================================
  protected function createSettingsModel(){
      return new Settings();
  }

  protected function installEventHandlers(){

    // Handler: Fields::EVENT_REGISTER_FIELD_TYPES
    Event::on(
      Fields::class,
      Fields::EVENT_REGISTER_FIELD_TYPES,
      function (RegisterComponentTypesEvent $event) {
        $event->types[] = AssetsPlusField::class;
      }
    );
  }
}
