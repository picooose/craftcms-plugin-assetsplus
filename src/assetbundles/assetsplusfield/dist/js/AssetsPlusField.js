Craft.AssetsPlusSelectInput = Craft.AssetSelectInput.extend({
  init: function(settings) {
    Craft.BaseElementSelectInput.prototype.init.call(this, settings);

    //Hidden field that contains the Assets groups
    this.$groupsInput = $(`[name="${this.settings.name.slice(0,-1)}-groups]"]`);
  },
  getElements: function() {
    return this.$elementsContainer.find('.element');
  },
  initElementSort: function() {
    if (this.settings.sortable) {
      const sortables = [...this.$elementsContainer.children()];

      sortables.forEach((group) => {
        this.createSortable(group);
      });

      // parent sortable
      this.$sortable = this.createSortable(this.$elementsContainer[0], {handle: true});

      //init
      this.prepGroups();

      //Updating sortables when removing an asset
      this.on('removeElements', () => { setTimeout(() => {
        this.updateGroups()
      }, 300)});
    }
  },
  addElements: function($elements) {
    this.thumbLoader.load($elements);

    if (this.settings.selectable) {
      this.elementSelect.addItems($elements);
    }

    if (this.settings.sortable) {
      const sortables = [...this.$elementsContainer.children()];
      sortables.forEach((el) => {
        if(!el.classList.contains('assetsplus--group')){
          //wrapping the element
          let wrapper = this.createWrapper({el: el});
          this.createSortable(wrapper);
          this.$elementsContainer.append(wrapper);
        }
      });

      if(this._initialized){
        this.updateGroups();
      }
    }

    if (this.settings.editable) {
        this._handleShowElementEditor = $.proxy(function(ev) {
            var $element = $(ev.currentTarget);
            if (Garnish.hasAttr($element, 'data-editable') && !$element.hasClass('disabled') && !$element.hasClass('loading')) {
                this.elementEditor = this.createElementEditor($element);
            }
        }, this);

        this.addListener($elements, 'dblclick', this._handleShowElementEditor);

        if ($.isTouchCapable()) {
            this.addListener($elements, 'taphold', this._handleShowElementEditor);
        }
    }

    $elements.find('.delete').on('click', $.proxy(function(ev) {
        this.removeElement($(ev.currentTarget).closest('.element'));
    }, this));

    this.$elements = this.$elements.add($elements);
    this.updateAddElementsBtn();
  },
  updateGroups: function(){
    //Update the field value
    const groups = [...this.$elementsContainer.children()].filter(group => group.children.length);
    const value = groups.map((group) => {
      return [...group.querySelectorAll('.element')].map(el => parseInt(el.getAttribute('data-id')));
    });

    this.$groupsInput[0].value = JSON.stringify(value);
    this.prepGroups();
  },
  prepGroups: function(){
    //removing empty groups
    [...this.$elementsContainer.children('span')].filter(el => el.children.length === 0).forEach(el => el.remove());
    [...this.$elementsContainer.children('span')].forEach((el) => {
      let wrapper = this.createWrapper();
      el.parentNode.insertBefore(wrapper, el.nextSibling);
      this.createSortable(wrapper, {group: true});
    });
  },
  createSortable: function(el, opts = {}){
    const options = {
			animation: 150,
			fallbackOnBody: true,
      swapThreshold: 0.8,
      easing: 'cubic-bezier(0.86, 0, 0.07, 1)',
      group: {
        put: (to, item) => {
          return !item.el.classList.contains('elements') && to.el.children.length < this.settings.groupsLimit;
        }
      },
      onSort: () => { this.updateGroups(); }
    };

    return new Sortable(el , options)
  },
  createWrapper: function(opts = {}){
    const wrapper = document.createElement('span');
    wrapper.classList.add('assetsplus--group');

    if(opts.el){ //wrapper
      opts.el.parentNode.insertBefore(wrapper, opts.el);
      wrapper.appendChild(opts.el);
    }

    return wrapper;
  }
});