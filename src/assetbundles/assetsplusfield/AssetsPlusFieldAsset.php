<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      https://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\assetbundles\assetsplusfield;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class AssetsPlusFieldAsset extends AssetBundle
{
  // Public Methods
  // =========================================================================

  /**
   * @inheritdoc
   */
  public function init()
  {
    $this->sourcePath = "@apstudio/assetsplus/assetbundles/assetsplusfield/dist";

    $this->depends = [
      CpAsset::class,
    ];

    $this->js = [
      'js/Sortable.min.js',
      'js/AssetsPlusField.js',
    ];

    $this->css = [
      'css/AssetsPlusField.css',
    ];

    parent::init();
  }
}
