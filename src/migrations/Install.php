<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      http://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\migrations;

use apstudio\assetsplus\AssetsPlus;

use Craft;
use craft\config\DbConfig;
use craft\db\Migration;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class Install extends Migration
{
  public $driver;

  public function safeUp()
  {
    $this->driver = Craft::$app->getConfig()->getDb()->driver;
    if ($this->createTables()) {
      $this->createIndexes();
      $this->addForeignKeys();
      // Refresh the db schema caches
      Craft::$app->db->schema->refresh();
      $this->insertDefaultData();
    }

    return true;
  }

  public function safeDown()
  {
    $this->driver = Craft::$app->getConfig()->getDb()->driver;
    $this->removeTables();

    return true;
  }

  // Protected Methods
  // =========================================================================
  protected function createTables()
  {
    $tablesCreated = false;

    $tableSchema = Craft::$app->db->schema->getTableSchema('{{%assetsplus}}');
    if ($tableSchema === null) {
      $tablesCreated = true;
      $this->createTable(
        '{{%assetsplus}}',
        [
          'id' => $this->primaryKey(),
          'dateCreated' => $this->dateTime()->notNull(),
          'dateUpdated' => $this->dateTime()->notNull(),
          'uid' => $this->uid(),
          'siteId' => $this->integer()->notNull(),
          'elementId' => $this->integer()->notNull(),
          'groups' => $this->text()->null(),
        ]
      );
    }

    return $tablesCreated;
  }

  protected function createIndexes()
  {
  }

  protected function addForeignKeys()
  {
    $this->addForeignKey(
      $this->db->getForeignKeyName('{{%assetsplus}}', 'siteId'),
      '{{%assetsplus}}',
      'siteId',
      '{{%sites}}',
      'id',
      'CASCADE',
      'CASCADE'
    );

    $this->addForeignKey(
      $this->db->getForeignKeyName('{{%assetsplus}}', 'elementId'),
      '{{%assetsplus}}',
      'elementId',
      '{{%elements}}',
      'id',
      'CASCADE',
      'CASCADE'
    );
  }

  protected function insertDefaultData()
  {
  }

  protected function removeTables()
  {
    $this->dropTableIfExists('{{%assetsplus}}');
  }
}
