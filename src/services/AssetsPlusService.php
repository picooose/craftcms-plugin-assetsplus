<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      https://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\services;

use apstudio\assetsplus\AssetsPlus;
use apstudio\assetsplus\records\AssetsPlusRecord;

use Craft;
use craft\base\Component;
use craft\base\ElementInterface;
use craft\elements\Asset;
use craft\events\AssetThumbEvent;
use craft\volumes\Local;

use craft\helpers\Json as JsonHelper;
use craft\helpers\FileHelper;

use yii\base\Exception;
use yii\validators\UrlValidator;
use yii\base\InvalidConfigException;

use mikehaertl\shellcommand\Command as ShellCommand;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class AssetsPlusService extends Component{
  // Assets grouping
  // =========================================================================
  protected static function getId(ElementInterface $element){
    $revision  = $element->getCurrentRevision();
    return $revision ? $revision->id: $element->id;
  }

  public static function saveGroups(ElementInterface $element, $data = []){
    $elementId = self::getId($element);
    $record    = AssetsPlusRecord::findOne(['elementId' => $elementId]);

    if(!$record){
      $record = new AssetsPlusRecord();
    }

    $record->setAttributes([
      'siteId' => $element->siteId,
      'elementId' => $elementId,
      'groups' => $data
    ]);

    if($record->validate()){
      $record->save();
    }
  }

  public static function getGroups(ElementInterface $element){
    $record    = AssetsPlusRecord::findOne(['elementId' => self::getId($element)]);
    return $record ? json_decode($record->groups) : [];
  }

  public static function getAssets(ElementInterface $element, $fieldHandle){
    if(!$element) return false;

    $res    = [];
    $assets = $element[$fieldHandle]->all();

    // Get field settings
    $field = $element->getFieldLayout()->getFieldByHandle($fieldHandle);

    if($field->allowGrouping){
      $groups = self::getGroups($element);
      $groupIdx = 0;

      // Reorganising assets in groups
      foreach($groups as $group){
        $groupCount = count($group);
        $res[] = array_slice($assets, $groupIdx, $groupCount);
        $groupIdx += $groupCount;
      }

      // Computing ratios
      foreach($res as $group){
        $groupRatio = 0;
        // Processing groupRatios
        foreach($group as $asset){
          $groupRatio += $asset->ratio;
        }

        // Assigning grouRatios
        foreach($group as $asset){
          $asset->groupRatio = $groupRatio;
        }
      }
    }else{
      $res = [$assets];
    }

    return $res;
  }
}
