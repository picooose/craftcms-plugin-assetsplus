<?php

namespace apstudio\assetsplus\models;

use craft\base\Model;
use craft\validators\ArrayValidator;

class Settings extends Model{

    // Static Methods
    // =========================================================================
    public function __construct(array $config = []){
        parent::__construct($config);
    }

    // Public Properties
    // =========================================================================

    public $ffmpegPath        = '/usr/bin/ffmpeg';
    public $ffprobePath       = '/usr/bin/ffprobe';
    public $ffprobeOptions    = '-v quiet -print_format json -show_format -show_streams';
    public $useHashedNames    = true;
    public $hashedNamesLength = 10;

    public $videoPaths = [
      'default'   => '@webroot/assets/videos/',
    ];

    public $videoUrls = [
      'default'   => '@web/assets/videos/',
    ];

    public $defaultThumbnailOptions = [
        'fileSuffix'     => '.jpg',
        'timeInSecs'     => 0,
        'width'          => '',
        'height'         => '',
    ];

    // Public Methods
    // =========================================================================

    public function init(){
        parent::init();
    }

    public function rules(){
        return [
            ['ffmpegPath', 'string'],
            ['ffmpegPath', 'required'],
            ['ffprobePath', 'string'],
            ['ffprobePath', 'required'],
            ['ffprobeOptions', 'string'],
            ['ffprobeOptions', 'safe'],
            ['videoPath', 'string'],
            ['videoPath', 'required'],
            ['videoPaths', ArrayValidator::class],
            ['videoPaths', 'required'],
            ['videoUrls', ArrayValidator::class],
            ['videoUrls', 'required'],
            ['useHashedNames', 'boolean'],
            ['useHashedNames', 'default', 'value' => false],
        ];
    }
}
