<?php

return [
    // The path to the ffmpeg binary
    'ffmpegPath' => '/usr/bin/ffmpeg',

    // The path to the ffprobe binary
    'ffprobePath' => '/usr/bin/ffprobe',

    // The options to use for ffprobe
    'ffprobeOptions' => '-v quiet -print_format json -show_format -show_streams',

    // The path where the transcoded videos are stored; must have a trailing /
    // Yii2 aliases are supported here
    'videoPaths' => [
      'default'   => '@webroot/assets/video/',
    ]
];
