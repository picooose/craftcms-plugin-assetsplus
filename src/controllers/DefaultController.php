<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      https://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\controllers;

use craft\web\Controller;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class DefaultController extends Controller{
}
