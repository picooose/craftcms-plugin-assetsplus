<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      http://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\records;

use craft\db\ActiveRecord;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class AssetsPlusRecord extends ActiveRecord{

  public static function tableName(){
    return '{{%assetsplus}}';
  }

  public function rules(){
      return [
          [['siteId', 'elementId'], 'required'],
          [['siteId', 'elementId'], 'integer'],
          [['groups'], 'string'],
      ];
  }
}
