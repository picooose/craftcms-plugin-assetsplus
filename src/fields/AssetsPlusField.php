<?php

/**
 * Assets Plus plugin for Craft CMS 3.x
 *
 * An Asset field with extra features
 *
 * @link      https://a-p.studio
 * @copyright Copyright (c) 2020 Adrien Picard
 */

namespace apstudio\assetsplus\fields;

use apstudio\assetsplus\elements\AssetPlus;
use apstudio\assetsplus\services\AssetsPlusService;
use apstudio\assetsplus\assetbundles\assetsplusfield\AssetsPlusFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\fields\Assets;
use yii\db\Schema;

/**
 * @author    Adrien Picard
 * @package   AssetsPlus
 * @since     1.0.0
 */
class AssetsPlusField extends Assets
{
  protected $settingsTemplate = 'assetsplus/_components/fields/AssetsPlusField_settings';
  protected $inputJsClass = 'Craft.AssetsPlusSelectInput';
  protected $groupsHandle;

  //Group settings
  public $allowGrouping = true;
  public $groupsLimit = 2;

  //Protected
  protected static function elementType(): string{
    return AssetPlus::class;
  }

  protected function getInputTemplate(){
    return $this->allowGrouping ?
    'assetsplus/_components/fields/AssetsPlusField_input' :
    '_components/fieldtypes/assets/input';
  }

  protected function getInputJsClass(){
    return $this->allowGrouping ? 'Craft.AssetsPlusSelectInput' : 'Craft.AssetSelectInput';
  }

  //Public
  public function init(){
    parent::init();

    $this->groupsHandle = $this->handle . '-groups';
  }

  public static function displayName(): string{
    return Craft::t('assetsplus', 'Assets Plus');
  }

  public function afterElementSave(ElementInterface $element, bool $isNew){
    $params = Craft::$app->getRequest()->getBodyParam('fields', []);
    if(isset($params[$this->groupsHandle])){
      AssetsPlusService::saveGroups($element, $params[$this->groupsHandle]);
    }

    return parent::afterElementSave($element, $isNew);
  }

  public function getInputVars($value, ElementInterface $element = null){
    // Register our asset bundle
    Craft::$app->getView()->registerAssetBundle(AssetsPlusFieldAsset::class);

    $variables                  = $this->inputTemplateVariables($value, $element);
    $variables['groups']        = AssetsPlusService::getGroups($element);
    $variables['groupsLimit']   = $this->groupsLimit;
    $variables['jsClass']       = $this->getInputJsClass();

    return $variables;
  }

  public function getInputHtml($value, ElementInterface $element = null): string{
    $variables = $this->getInputVars($value, $element);

    return Craft::$app->getView()->renderTemplate($this->getInputTemplate(), $variables);
  }

  public function getStaticHtml($value, ElementInterface $element): string{
    $variables = $this->getInputVars($value, $element);
    $variables['static'] = true;

    return Craft::$app->getView()->renderTemplate($this->getInputTemplate(), $variables);
  }
}
